package com.task_11thsept.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.task_11thsept.R;
import com.task_11thsept.beans.Feed;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedDetailsActivity extends AppCompatActivity {
    @BindView(R.id.iv_round_image)
    ImageView ivRoundImage;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_url)
    TextView tvUrl;
    @BindView(R.id.tv_repo_name)
    TextView tvRepoName;
    @BindView(R.id.tv_repo_description)
    TextView tvRepoDescription;
    @BindView(R.id.tv_repo_url)
    TextView tvRepoUrl;
    private Feed feed;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_feeds_detail);
        ButterKnife.bind(this);
        feed=(Feed)getIntent().getSerializableExtra("DATA");
        showPassedData();
    }

    private void showPassedData(){
        tvName.setText(feed.getName());
        tvUsername.setText(feed.getUsername());
        tvUrl.setText(feed.getUrl());
        tvRepoName.setText(feed.getRepo().getName());
        tvRepoDescription.setText(feed.getRepo().getDescription());
        tvRepoUrl.setText(feed.getRepo().getUrl());
        Glide.with(this).load(feed.getAvatar()).into(ivRoundImage);
    }
}
