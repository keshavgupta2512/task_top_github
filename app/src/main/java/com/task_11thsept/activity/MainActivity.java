package com.task_11thsept.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.task_11thsept.R;
import com.task_11thsept.adapters.FeedsListAdapter;
import com.task_11thsept.beans.Feed;
import com.task_11thsept.beans.FeedsInfoNews;
import com.task_11thsept.responseapi.ApiResponse;
import com.task_11thsept.utilities.Constant;
import com.task_11thsept.utilities.MyApplication;
import com.task_11thsept.utilities.OnItemClickListener;
import com.task_11thsept.viewmodel.FeedsViewModel;
import com.task_11thsept.viewmodelfactory.ViewModelFactory;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.annotations.Nullable;

public class MainActivity extends AppCompatActivity implements OnItemClickListener.OnItemClickCallback {

    @Inject
    ViewModelFactory viewModelFactory;


    @BindView(R.id.rlv_feeds)
    RecyclerView rlvFeeds;

    FeedsViewModel viewModel;

    AlertDialog progressDialog;
    private List<Feed> arrayFeeds;
    private AppCompatActivity activity;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeds);
        progressDialog = Constant.getProgressDialog(this, "Please wait...");

        ButterKnife.bind(this);
        activity = MainActivity.this;
        setUpFeedsRecyclerView();
        ((MyApplication) getApplication()).getAppComponent().doInjection(this);

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(FeedsViewModel.class);
        hitApiCall();

        viewModel.feedsResponse().observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                consumeResponse(apiResponse);
            }
        });
    }

    private void setUpFeedsRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(activity);
        DividerItemDecoration verticalDecoration = new DividerItemDecoration(activity,
                DividerItemDecoration.VERTICAL);
        Drawable verticalDivider = ContextCompat.getDrawable(activity, R.drawable.divider_vertical_language);
        verticalDecoration.setDrawable(verticalDivider);
        rlvFeeds.addItemDecoration(verticalDecoration);
        rlvFeeds.setLayoutManager(linearLayoutManager);
    }

    public void hitApiCall() {
        if (!Constant.checkInternetConnection(this)) {
            Toast.makeText(activity, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        } else {
            viewModel.hitFeedsApi("java", "weekly");
        }
    }

    private void consumeResponse(ApiResponse apiResponse) {

        switch (apiResponse.status) {

            case LOADING:
                progressDialog.show();
                break;

            case SUCCESS:
                progressDialog.dismiss();
                renderSuccessResponse(apiResponse.data);
                break;

            case ERROR:
                progressDialog.dismiss();
                Toast.makeText(activity, getResources().getString(R.string.invalid_response), Toast.LENGTH_SHORT).show();
                break;

            default:
                break;
        }
    }

    /*
     * method to handle success response
     * */
    private void renderSuccessResponse(JsonElement response) {
        arrayFeeds = new ArrayList<>();
        if (!response.isJsonNull()) {
            Log.d("response=", response.toString());
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("Feeds", response);
            Log.e("RES", jsonObject.toString());
            FeedsInfoNews feedsInfoNews = new Gson().fromJson(jsonObject.toString(), FeedsInfoNews.class);
            if (!feedsInfoNews.getFeeds().isEmpty()) {
                Toast.makeText(activity, "feedsLoaded", Toast.LENGTH_SHORT).show();
                arrayFeeds = feedsInfoNews.getFeeds();
                setFeedsAdapter(arrayFeeds);
            } else {
                Toast.makeText(activity, "No feeds Loaded", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(activity, getResources().getString(R.string.invalid_response), Toast.LENGTH_SHORT).show();
        }
    }

    private void setFeedsAdapter(List<Feed> arrayFeeds) {
        FeedsListAdapter feedsListAdapter = new FeedsListAdapter(activity, arrayFeeds, this);
        rlvFeeds.setAdapter(feedsListAdapter);
    }

    @Override
    public void onItemClicked(View view, int position) {
       Intent intent=new Intent(activity,FeedDetailsActivity.class);
       intent.putExtra("DATA",arrayFeeds.get(position));
       startActivity(intent);

    }
}
