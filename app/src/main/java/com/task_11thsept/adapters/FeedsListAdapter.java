package com.task_11thsept.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.task_11thsept.R;
import com.task_11thsept.beans.Feed;
import com.task_11thsept.imagecache.ImageLoader;
import com.task_11thsept.utilities.OnItemClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FeedsListAdapter extends RecyclerView.Adapter<FeedsListAdapter.MyViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private List<Feed> arrayList;
    private OnItemClickListener.OnItemClickCallback onItemClickCallback;
    private ImageLoader imageLoader;

    public FeedsListAdapter(Context context, List<Feed> arrayList,
                            OnItemClickListener.OnItemClickCallback onItemClickCallback) {
        this.context = context;
        this.arrayList = arrayList;
        imageLoader = new ImageLoader(context);
        this.inflater = LayoutInflater.from(context);
        this.onItemClickCallback = onItemClickCallback;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.inflate_layout_feeds, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tvReponame.setText(arrayList.get(position).getRepo().getName());
        holder.tvUsername.setText(arrayList.get(position).getUsername());
        holder.rlvMainContent.setOnClickListener(new OnItemClickListener(position,onItemClickCallback));
        Glide.with(context).load(arrayList.get(position).getAvatar()).into(holder.ivRoundImage);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_round_image)
        ImageView ivRoundImage;
        @BindView(R.id.tv_reponame)
        TextView tvReponame;
        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.rlv_main_content)
        ConstraintLayout rlvMainContent;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}

