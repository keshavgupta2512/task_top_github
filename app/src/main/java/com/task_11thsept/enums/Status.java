package com.task_11thsept.enums;

public enum Status {
    LOADING,
    SUCCESS,
    ERROR,
    COMPLETED
}