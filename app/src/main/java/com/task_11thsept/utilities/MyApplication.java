package com.task_11thsept.utilities;

import android.app.Application;
import android.content.Context;

import com.task_11thsept.daggerconfig.AppComponent;
import com.task_11thsept.daggerconfig.AppModule;
import com.task_11thsept.daggerconfig.DaggerAppComponent;
import com.task_11thsept.daggerconfig.UtilsModule;

public class MyApplication extends Application {
    AppComponent appComponent;
    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).utilsModule(new UtilsModule()).build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }
}
