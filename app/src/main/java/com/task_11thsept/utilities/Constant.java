package com.task_11thsept.utilities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.task_11thsept.R;

public class Constant {

    public static androidx.appcompat.app.AlertDialog getProgressDialog(Context context, String text){
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        androidx.appcompat.app.AlertDialog dialog = builder.create();
        View dialogLayout;
        dialogLayout = LayoutInflater.from(context).inflate(R.layout.custom_progress_bar, null);
        TextView text1=(TextView)dialogLayout.findViewById(R.id.loading_msg);
        text1.setText(text);
        dialog.setView(dialogLayout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    public static boolean checkInternetConnection(Activity context) {

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }
}
