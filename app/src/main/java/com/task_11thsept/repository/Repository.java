package com.task_11thsept.repository;

import com.google.gson.JsonElement;
import com.task_11thsept.interfaces.ApiCallInterface;

import io.reactivex.Observable;

public class Repository {

    private ApiCallInterface apiCallInterface;

    public Repository(ApiCallInterface apiCallInterface) {
        this.apiCallInterface = apiCallInterface;
    }

    /*
     * method to call login api
     * */
    public Observable<JsonElement> executeLogin(String mobileNumber, String password) {
        return apiCallInterface.login(mobileNumber, password);
    }

}