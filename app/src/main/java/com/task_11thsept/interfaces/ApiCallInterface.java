package com.task_11thsept.interfaces;

import com.google.gson.JsonElement;
import com.task_11thsept.utilities.Urls;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiCallInterface {

    @GET(Urls.FEEDS_END_POINT)
    Observable<JsonElement> login(@Query("language") String language, @Query("since") String time);
}