package com.task_11thsept.viewmodelfactory;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.task_11thsept.repository.Repository;
import com.task_11thsept.viewmodel.FeedsViewModel;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private Repository repository;

    @Inject
    public ViewModelFactory(Repository repository) {
        this.repository = repository;
    }


    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(FeedsViewModel.class)) {
            return (T) new FeedsViewModel(repository);
        }
        throw new IllegalArgumentException("Unknown class name");
    }
}